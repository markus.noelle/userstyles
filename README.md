# Darcula-Inspired Website themes

Themes for (mostly dev-related) websites.

Inspired by Jetbrains' Darcula Theme, written in LESS.

## Building the Themes

### Prerequisites

- `npm` or `yarn`
- grunt.js
- (Optional) Visual Studio Code
 

### Project Setup

1. Clone repo
1. Install dependencies via `npm` or `yarn`

### Build everything

Console: Run `grunt` or `grunt less`

VS Code: The build task is set as default build task.

### Rebuild on file change

Console: Run `grunt watch` 

VS Code: Run the task `grunt:watch`

