module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            dev:{
                options: {
                    paths: ['src/common']
                },
                files: [
                    {
                        expand: true,
                        cwd: 'src',
                        src: ['*.less'],
                        dest: 'out/dev',
                        ext: '.css'
                    }
                ]
            },
            prod:{
                options: {
                    paths: ['src/common']
                },
                files: [
                    {
                        expand: true,
                        cwd: 'src/scopedefs',
                        src: ['*.less'],
                        dest: 'out/importable',
                        ext: '.css'
                    }
                ]
            }
        },
        watch: {
            files: ['./src/*.less', './src/common/*.less'],
            tasks: ["less:dev"]
        }
    });
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['less:dev']);
    grunt.registerTask('prod', ['less:prod']);
}